## Hi there!

This app fetches data from a Shopping API. Results can be sortable by price.

App is created with:
- Webpack 4
- ReactJS, Redux, Redux-Saga
- Shopping API

Inside the project, you can run some built-in commands:

### 1.) Install Node modules:
In root folder run:
        `yarn install`


### 2.) Run app in development mode:
In root folder run: `yarn run dev`

        ...when compiling is done by Webpack, the page will automatically open browser at
        http://0.0.0.0:1234 via Webpack-Dev-Server.

        (Local: http://localhost:1234 works as well)

### 3.) Build production-ready code and run app:
In root folder run: `yarn run prod`

    ...when compiling is done by Webpack, the page will automatically open browser on your network at
    http://your-local-ip:5678 via Node-Express Server.

    It builds the app for production inside the newly created folder 'PRODUCTION-READY'.
    Resources (JS, CSS, HTML) are minified and filenames include the hashes.


### Also,

- App is fully-responsive (from mobile -> desktop).
- Live Demo can be seen here: https://bit.ly/2ww2YT8



.

Happy hacking!

Marko Benigar
Email: markobenigar@gmail.com
