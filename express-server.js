/********************************************************************
*                                                                   *
*    Node - EXPRESS HTTP WEB SERVER                                 *
*           EXPRESS - the server which will host the react app      *
*                                                                   *
*    For testing PROD code & Service Workers                        *
*    By Marko Benigar, 2018                                         *
*                                                                   *
*  You can also start a DEFAULT MAC HTTP WEB SERVER with command:   *
*    1.) $ python -m SimpleHTTPServer 8000 (in index.html folder)   *
*    2.) Open browser at http://localhost:8000                      *                                                                          *
********************************************************************/

const path = require('path');
const opn = require('opn');
const express = require('express');
const dns = require('dns');
const os = require('os');

const app = express();

app.use(express.static(__dirname + '/PRODUCTION-READY'));

const protocol = 'http';
const hostname = 'localhost';
const port = 5678;

app.listen(port, function() {
    console.log('------------------------------------------------------------------');
    console.log('Ready, the server is running on port: ' + port);
    console.log('\x1b[32m%s\x1b[0m', '1.) Access server at: ' + protocol + '://' + hostname + ':' + port);
});

app.get('/', function (req, res) {
    res.redirect('/index.html');
});

dns.lookup(os.hostname(), function (err, IP, fam) {
  console.log('\x1b[32m%s\x1b[0m', '2.) Access server at: ' + protocol + '://' + IP + ':' + port);
  console.log('------------------------------------------------------------------');
  opn(protocol + '://' + IP + ':' + port);
})
