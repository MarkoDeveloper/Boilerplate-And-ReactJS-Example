import React from 'react';
import Header from '../header/Header.jsx';
import Main from '../main/Main.jsx';
import Footer from '../footer/Footer.jsx'
import styles from '../../app-styles-global/app.styles.global.scss';

const App = () => {

    return (
        <div className={styles.appWrapper}>
            <Header />
            <Main />
            <Footer/>
        </div>
    )
}
export default App;
