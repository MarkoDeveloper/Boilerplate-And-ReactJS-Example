import React from 'react';
import { connect } from 'react-redux'
import styles from './dataContainer.scss';
import Image from '../image/Image.jsx';
import { getApiProductData } from '../../store/actions/index.js'

class DataContainer extends React.Component {

    constructor(props) {
        super(props);
    }

    getImageURL = imageHash => {
        const url = 'https://cdn.aboutstatic.com/file/' + imageHash + '?width=400&height=400&quality=75&bg=ffffff&progressive=1';
        return url;
    }

    getFormattedPrice = price => {
        const formattedPrice = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(price/100);
        return formattedPrice;
    }

    componentDidMount() {
        this.props.handle_getApiProductData();
    }

    render() {

        return (

             this.props.data.payload ?

                <div className={styles.dataContainerComponent}>
                    <div className={styles.contentWrapper}>

                        {this.props.data.payload.map((item, index) =>
                          <div key={index} className={item.product.id}>
                              <Image imgURL={this.getImageURL(item.product.image.hash)}
                                     imgTitle={item.product.name}
                                     imgAlt={item.product.name}
                               />
                           <div className={styles.productInfo}>
                               <p><span>Name:&nbsp;</span>{item.product.name}</p>
                               <p><span>Brand:&nbsp;</span>{item.product.brand.name}</p>
                               <p><span>Price:&nbsp;</span><span className={styles.price}>{this.getFormattedPrice(item.product.max_price)}</span></p>
                           </div>
                          </div>
                        )}

                    </div>
                </div> :
                null
        );
    }
}

//Define which data is needed from the Store
const mapStateToProps = state => (
    { data: state }
)

//Use Action Creators in this component
const mapDispatchToProps = dispatch => (
    { handle_getApiProductData: () => dispatch(getApiProductData()) }
)

//Connect component to Redux Store
export default DataContainer = connect(mapStateToProps, mapDispatchToProps)(DataContainer);
