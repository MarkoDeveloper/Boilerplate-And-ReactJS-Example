import React from 'react';
import styles from './footer.scss';

const Footer = () => {

    return (
        <footer className={styles.globalFooter}>
            <div className={styles.contentWrapper}>
            </div>
        </footer>
    );
}
export default Footer;
