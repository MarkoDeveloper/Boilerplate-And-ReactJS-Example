// @flow
import React from 'react';
import styles from './header.scss';
import Image from '../image/Image.jsx';
import SortButtons from '../sortButtons/SortButtons.jsx';
import Logo from '../../assets/img/shopping-bags.jpg';

const Header = () => {

    return (
        <header>
            <div className={styles.contentWrapper}>
                <div className={styles.logoWrapper}>
                    <Image imgURL={Logo}
                           imgTitle={'Let\'s go shopping!'}
                           imgAlt={'Let\'s go shopping!'}
                     />
                </div>
                <div className={styles.authorIntro}>Developed by Marko Benigar</div>
                <div className={styles.techIntro}>Shopping API + ReactJS + Redux + Redux-Saga</div>

                <SortButtons />
            </div>
        </header>
    );
}
export default Header;
