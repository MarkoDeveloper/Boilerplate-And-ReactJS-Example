// @flow
import React from 'react';
import styles from './image.scss';

type Props = {
    imgURL: string,
    imgTitle: string,
    imgAlt: string
}

const Image = (props:Props) => {

    return (
        <img className={styles.responsiveImg}
             src={props.imgURL}
             title={props.imgTitle}
             alt={props.imgAlt} >
        </img>
    );
}
export default Image;
