import React from 'react';
import styles from './main.scss';
import Section1 from '../section1/Section1.jsx'

const Main = () => {

    return (
        <main>
            <Section1 />
        </main>
    );
}
export default Main;
