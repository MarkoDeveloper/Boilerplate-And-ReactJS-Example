import React from 'react';
import styles from './section1.scss';
import DataContainer from '../dataContainer/DataContainer.jsx';

const Section1 = () => {

    return (
        <section id={styles.section1}>
            <div className={styles.wrapperContent}>
                <DataContainer />
            </div>
        </section>
    );
}
export default Section1;
