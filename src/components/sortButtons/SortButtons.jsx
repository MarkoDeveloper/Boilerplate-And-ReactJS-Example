import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './sortButtons.scss';
import { sortProductsByLowestPrice, sortProductsByHighestPrice } from '../../store/actions/index.js'

class SortButtons extends React.Component {

    constructor(props) {
        super(props);
    }

    compareLowest = (a,b) => {
          const genreA = a.product.max_price;
          const genreB = b.product.max_price;

          let comparison = 0;

          if (genreA > genreB) {
            comparison = 1;
          } else if (genreA < genreB) {
            comparison = -1;
          }

          return comparison;
    }

    sortByLowestPrice = () => {
        if(this.props.data.payload) {
            const sortedData = this.props.data.payload.sort(this.compareLowest);
            this.props.handle_sortProductsByLowestPrice(sortedData);
            return;
        }
    }

    sortByHighestPrice = () => {
        if(this.props.data.payload) {
            const sortedData = this.props.data.payload.sort(this.compareLowest).reverse(); //reverse to highest price
            this.props.handle_sortProductsByHighestPrice(sortedData);
            return;
        }
    }


    render() {

        return (
            <div className={styles.sortButtonsComponent}>
                <div className={styles.contentWrapper}>
                    <button onClick={this.sortByLowestPrice}>Sort&nbsp;from lowest&nbsp;price</button>
                    <button onClick={this.sortByHighestPrice}>Sort&nbsp;from highest&nbsp;price</button>
                </div>
            </div>
        );
    }
}

SortButtons.propTypes = {
    data: PropTypes.object.isRequired
}

//Define which data is needed from the Store
const mapStateToProps = state => (
    { data: state }
)


//Use Action Creators in this component
const mapDispatchToProps = dispatch => (
    {
      handle_sortProductsByLowestPrice: sortedData => dispatch(sortProductsByLowestPrice(sortedData)),
      handle_sortProductsByHighestPrice: sortedData => dispatch(sortProductsByHighestPrice(sortedData))
    }
)

//Connect component to Redux Store
export default SortButtons = connect(mapStateToProps,mapDispatchToProps)(SortButtons);
