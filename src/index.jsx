/*
A polyfilled environment for React 16:
Use babel-polyfill for React to support IE < 11
Read more:
https://reactjs.org/docs/javascript-environment-requirements.html
https://babeljs.io/docs/en/babel-polyfill/
*/
import "babel-polyfill";

import React from 'react';
import ReactDOM from 'react-dom';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import Reducer from './store/reducers/index.js';
import App from './components/app/App.jsx';
import RootSaga from './store/sagas/saga.js';
import Logger from 'redux-logger';

//Create saga middleware
const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

//Add logger (console output) to Middleware for development build only
if (process.env.NODE_ENV === 'development') {
    middlewares.push(Logger);
}

//Create Redux Store and inject Reducer & Middleware to the store
const store = createStore(Reducer, applyMiddleware(...middlewares));

//Run saga
sagaMiddleware.run(RootSaga);

const reactContainer = document.getElementById('app');

//Wrap & pass store
const rootComponent = (
    <Provider store={store}>
        <App/>
    </Provider>
)
//Render React Root-Component into Real DOM
ReactDOM.render(rootComponent, reactContainer);
