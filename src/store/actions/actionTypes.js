export const REQUEST_API_DATA_PRODUCTS = 'REQUEST_API_DATA_PRODUCTS';
export const SORT_BY_LOWEST_PRICE = 'SORT_BY_LOWEST_PRICE';
export const SORT_BY_HIGHEST_PRICE = 'SORT_BY_HIGHEST_PRICE';
