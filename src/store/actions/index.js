/*****************************
*       Action Creators      *
*****************************/

import * as types from "./actionTypes";


export const getApiProductData = () => ({
  type: types.REQUEST_API_DATA_PRODUCTS
  //payload
});

export const sortProductsByLowestPrice = sortedData => ({
  type: types.SORT_BY_LOWEST_PRICE,
  payload: sortedData
});

export const sortProductsByHighestPrice = sortedData => ({
  type: types.SORT_BY_HIGHEST_PRICE,
  payload: sortedData
});
