
const fetchDataProductIds = async () => {

    try {
        const response = await fetch('https://m.aboutyou.de/api/products/v3/20250?appId=428&limit=48&page=1&sort=');
        return await response.json();
    }
    catch(error) {
        console.error({fetchDataProductIds: false, error});
    }
}

export default fetchDataProductIds;
