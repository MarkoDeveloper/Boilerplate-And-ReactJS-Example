
const fetchDataProducts = async (fetched_ids_data) => {

    try {
        const response = await fetch('https://m.aboutyou.de/api/products/v2/getProducts?productId=' + fetched_ids_data.productIds);
        return await response.json();
    }
    catch(error) {
        console.error({fetchDataProducts: false, error});
    }
}

export default fetchDataProducts;
