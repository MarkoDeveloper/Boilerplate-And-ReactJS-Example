import * as types from "./reducerTypes";


const reducer = (state = {/* default empty state, for now */}, action) => {

  switch (action.type) {

    case types.REQUEST_API_DATA_PRODUCTS:
      return {
          ...state,
          requesting: true
      };

    case types.PRODUCTS_RECEIVED:
      return {
          ...state,
          payload: action.payload,
          requesting: false
      };

    case types.SORT_BY_LOWEST_PRICE:
      return {
          ...state,
          payload: action.payload,
      };

    case types.SORT_BY_HIGHEST_PRICE:
      return {
          ...state,
          payload: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
