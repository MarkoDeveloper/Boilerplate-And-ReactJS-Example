import { call, put, takeLatest, all } from 'redux-saga/effects';
import * as types from "../reducers/reducerTypes";
import fetchDataProductIds from '../apis/fetchDataProductIds.js';
import fetchDataProducts from '../apis/fetchDataProducts.js';

//Saga
function* getApiProductData() {

  try {
      //Tell redux-saga to call fetch
      const fetched_ids_data = yield call(fetchDataProductIds);
      const fetched_products_data = yield call(fetchDataProducts, fetched_ids_data);

      //Tell redux-saga to dispatch an action
      yield put({type: types.PRODUCTS_RECEIVED, payload: fetched_products_data});
  }
  catch(error) {
      console.error({getApiProductData: false, error});
  }
}

//Watch for every REQUEST_API_DATA_PRODUCTS dispatched action
function* actionWatcher() {
  yield takeLatest(types.REQUEST_API_DATA_PRODUCTS, getApiProductData)
}


export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}
