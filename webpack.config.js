/*****************************************************************
*                                                                *
*                 WEBPACK 4 Config file                          *
*                 by Marko Benigar                               *
*                 2018                                           *                                                                                *
*                                                                *
*****************************************************************/

const path = require('path');
const webpack = require('webpack');
const DashboardPlugin = require('webpack-dashboard/plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const WebpackNotifierPlugin = require('webpack-notifier');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = env => {

    //default mode
    const MODE = {
        isDev: true,
        isProd: false
    };

    if (env.mode === 'production') {
        MODE.isDev = !MODE.isDev;
        MODE.isProd = !MODE.isProd;
    }

    const PUBLIC_PATH = 'https://www.MarkoBenigar.com/';

  return {
      entry: {
          indexPage: ['babel-polyfill', './src/index.jsx']
      },
      output: {
          path: path.resolve(__dirname, 'PRODUCTION-READY'),
          filename: MODE.isDev ? '[name].bundle.js' : '[name].[chunkhash].bundle.js'
      },
      devtool: MODE.isDev ? 'inline-source-map' : false,
      devServer: {
        contentBase: false,
        publicPath: '/',
        port: 1234,
        host: '0.0.0.0',
        compress: true,
        overlay: true,
        open: true,
        hot: true,
        historyApiFallback: true,
      },
      resolve: {
          extensions: [ '*', '.js', '.jsx', '.json', '.jpg', 'jpeg', '.png', '.gif', '.scss', '.css' ],
          modules: [ 'src', 'node_modules' ]
      },
      //modules config
      module: {
          rules: [
              {   //sass, scss, css
                  test: /\.(sa|sc|c)ss$/,
                  include: [
                      path.resolve(__dirname, 'src')
                    ],
                    //loaders chaining
                    use: [
                      //Step 4
                      {loader: MODE.isDev ? 'style-loader' : MiniCssExtractPlugin.loader},
                      //Step 3
                      {loader: 'css-loader',
                          options: {
                              sourceMap: MODE.isDev ? true : false,
                              modules: true,
                              localIdentName: '[local]'
                          }
                       },
                       //Step 2
                       {loader: 'postcss-loader',
                            options: {
                                sourceMap: MODE.isDev ? true : false
                            }
                       },
                       //Step 1
                       {loader: 'sass-loader',
                            options: {
                                sourceMap: MODE.isDev ? true : false
                            }
                        }
                    ]
              },
              // JS, JSX loader
              {   test: /\.(js|jsx)$/,
                  include: [
                      path.resolve(__dirname, 'src')
                  ],
                  loader: 'babel-loader'
              },
              // File loader for IMAGES
              {
                  test: /\.(jpg|jpeg|png|gif|svg)$/,
                  include: [
                      path.resolve(__dirname, 'src/assets')
                  ],
                  use: {
                      loader: 'file-loader',
                      options: {
                          name:  MODE.isDev ? '[path][name].[ext]' : '[path][name].[hash].[ext]',
                      },
                  },
              },
              // File loader for FONTS
              {
                  test: /\.(woff|woff2|ttf|eot)$/,
                  include: [
                      path.resolve(__dirname, 'src/assets')
                  ],
                  use: {
                      loader: 'file-loader',
                      options: {
                          name: MODE.isDev ? '[path][name].[ext]' : '[path][name].[hash].[ext]',
                      },
                  },
              },
              //File loader for VIDEO
              {
                 test: /\.mp4/,
                 include: [
                     path.resolve(__dirname, 'src/assets')
                 ],
                 use: {
                   loader: 'file-loader',
                   options: {
                     limit: 100000,
                     mimtetype: 'video/mp4',
                     name: MODE.isDev ? '[path][name].[ext]' : '[path][name].[hash].[ext]'
                   }
                 }
               }
          ]
      },
      //init, activate plugins
      plugins: [
            new WebpackNotifierPlugin({
                title: 'WEBPACK',
                excludeWarnings: true,
                contentImage: path.join(__dirname, '/src/assets/img/webpackNotifierPlugin/123060-004-CFD42765.jpg')
            }),
            new DashboardPlugin(),
          ...(MODE.isDev ? [
              new webpack.HotModuleReplacementPlugin(),
              new webpack.NamedModulesPlugin(),
              new HtmlWebpackPlugin({
                  filename: 'index.html',
                  template: './public/index.html',
                  cache: false,
                  showErrors: true,
                  chunks: ['indexPage']
              }),
          ] : []),
          ...(MODE.isProd ? [
              new FaviconsWebpackPlugin({
                      logo: './src/assets/img/shopping-bags.jpg',
                      persistentCache: false,
                      title: 'Marko Benigar Homepage - PWA',
                      inject: true,
                      icons: {
                          android: true,
                          appleIcon: true,
                          appleStartup: true,
                          coast: false,
                          favicons: true,
                          firefox: true,
                          opengraph: true,
                          twitter: true,
                          yandex: false,
                          windows: false
                      }
              }),
              new MiniCssExtractPlugin({
                  filename: MODE.isDev ? '[name].bundle.css' : '[name].[contenthash].bundle.css',
                  chunkFilename: MODE.isDev ? '[id].bundle.css' : '[id].[contenthash].bundle.css',
              }),
              new ManifestPlugin({
                fileName: 'asset-manifest.json',
               }),
              //SERVICE WORKER
              new SWPrecacheWebpackPlugin(
                  {
                    cacheId: 'PWA-CacheId-Marko',
                    dontCacheBustUrlsMatching: /\.\w{8}\./,
                    filename: 'ServiceWorker-Marko.js',
                    minify: true,
                    navigateFallback: PUBLIC_PATH + 'index.html',
                    staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
                  }
                ),
              //MINIFY CSS FOR PRODUCTION
              new OptimizeCssAssetsPlugin({
                  assetNameRegExp: /bundle.css$/,
                  cssProcessor: require('cssnano'),
                  cssProcessorOptions: {
                          discardComments: {
                              removeAll: true
                          }
                  },
                  canPrint: false
              }),
              new HtmlWebpackPlugin({
                  filename: 'index.html',
                  template: './public/index.html',
                  cache: false,
                  showErrors: true,
                  //MINIFY HTML FOR PRODUCTION
                  minify: {
                      collapseInlineTagWhitespace: true,
                      collapseWhitespace: true,
                      removeComments: true
                  },
                  chunks: ['indexPage']
              }),
              new WebpackPwaManifest({
                   name: 'Marko Benigar',
                   short_name: 'Marko Benigar',
                   description: 'I\'m Marko - Software Developer',
                   background_color: '#fff',
                   'theme-color': '#fc0045',
                   orientation: 'portrait',
                   display: 'standalone',
                   start_url: '',
                   includeDirectory: true,
                   inject: true,
                   ios: true,
                   icons: [
                     {
                       src: path.resolve('src/assets/img/shopping-bags.jpg'),
                       sizes: [192, 512]
                     }
                   ]
                 })
          ] : []),
      ],
      optimization: {
        minimizer: [
            //MINIFY JS FOR PRODUCTION
            new UglifyJsPlugin({
                test: /\.js$/i,
                sourceMap: MODE.isDev ? true : false,
                parallel: true,
                exclude: [/node_modules/],
                uglifyOptions: {
                  compress: {
                      drop_console: true,
                      keep_infinity: true
                  },
                  mangle: {
                      safari10: true
                  },
                  keep_fnames: true,
                  safari10: true,
                  output: {
                    beautify: false,
                    comments: false,
                    webkit: true
                  }
                }
            })
        ]
      }
  }
}
